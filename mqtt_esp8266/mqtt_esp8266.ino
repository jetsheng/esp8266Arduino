/*
 Basic ESP8266 MQTT example

 This sketch demonstrates the capabilities of the pubsub library in combination
 with the ESP8266 board/library.

 It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" every two seconds
  - subscribes to the topic "inTopic", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "inTopic" is an 1, switch ON the ESP Led,
    else switch it off

 It will reconnect to the server if the connection is lost using a blocking
 reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
 achieve the same result without blocking the main loop.

 To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"

*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

//===============Wifi==================================
const char* ssid = "lol2";
const char* password = "12345678";
const char* mqtt_server = "216.155.135.153";//216.155.135.153
const int location_id =465188498 ;
const int number_boxes=3;
//======================================================

//====================================================
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
char boxN[50];
char boxM[number_boxes][50];
int value = 0;
//===================================================

//================constant value========================
const int L1=16;
const int L2=4;
const int L3=5;
const int S1 = 13;
const int S2 = 15;
const int S3 = 14;

int state1;                        // 0 close - 1 open switch
int state2; 
int state3; 
int OldS1;
int OldS2=0;
int OldS3=0;
//=======================================================

void setup() {
  pinMode(L1, OUTPUT);
  pinMode(L2, OUTPUT);
  pinMode(L3, OUTPUT);
  pinMode(S1, INPUT);
  pinMode(S2, INPUT);
  pinMode(S3, INPUT);
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

for(int j=0; j<number_boxes; j++){
  snprintf(boxM[j],75,"cmnd/%d/%d",location_id,j+1);
}
if(strcmp(topic,boxM[0])== 0){
   if ((char)payload[0] == 'o'){
    digitalWrite(L1, HIGH);  
    delay(2000);
    digitalWrite(L1, LOW);  
  } 
}
if(strcmp(topic,boxM[1])== 0){
   if ((char)payload[0] == 'o'){
     digitalWrite(L2, HIGH);  
    delay(2000);
    digitalWrite(L2, LOW);  
  } 
}
if(strcmp(topic,boxM[2])== 0){
   if ((char)payload[0] == 'o'){
    digitalWrite(L3, HIGH);  
    delay(2000);
    digitalWrite(L3, LOW);  
  } 
}
/*
if(strcmp(topic,"smartbox/stat/box 1")== 0){
  publishIfDiff(state1,1);
}
if(strcmp(topic,"smartbox/stat/box 2")== 0){
  publishIfDiff(state2,2);
}
if(strcmp(topic,"smartbox/stat/box 3")== 0){
  publishIfDiff(state3,3);
}*/
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      for(int j=0; j<number_boxes; j++){
        snprintf(boxM[j],75,"cmnd/%d/%d",location_id,j+1);
        client.subscribe(boxM[j]);
      }
//      client.subscribe("cmnd/465188498/1");
//      client.subscribe("cmnd/465188498/2");
//      client.subscribe("cmnd/465188498/3");
      client.subscribe("smartbox/stat/box 1");
      client.subscribe("smartbox/stat/box 2");
      client.subscribe("smartbox/stat/box 3");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
/*
void publishIfDiff(int state,int box){
  snprintf (boxN, 75, "smartbox/stat/box %d",box );
  if (state == HIGH){
    snprintf (msg, 75, "The locker %d is closed",box );
    client.publish(boxN, msg);
  //  digitalWrite(5, ~state);   
  }
  else{
    snprintf (msg, 75, "The locker %d is opened",box );
    client.publish(boxN, msg);
    //digitalWrite(5, ~state);   
  }
  delay(20);
}
*/
void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  state1 = digitalRead(S1);
  state2 = digitalRead(S2);
  state3 = digitalRead(S3);

 /*
  if(OldS1!=state1)
    publishIfDiff(state1,1);
  if(OldS2!=state2)
    publishIfDiff(state2,2);
  if(OldS3!=state3)
    publishIfDiff(state3,3);
*/
  
  OldS1=state1;
  OldS2=state2;
  OldS3=state3;

  delay(20);


}
